print ("Hello World")    #Hello world is string, () -parenthesis,
print ('Hello world')    #double/single quote also used, 


print ("hello 'world' world")  #no error
print ('hello "world" world')  #no error

# print ("Hello "world" world") #error
# print ("Hello "World" world")  #error