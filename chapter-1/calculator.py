print (2+3*4)

print (2/4)  #floating point division
print (4/2)


print (4//2) #integer division
print (2//4) #integer division

print (2**0.5) #sqaure root of 2, 1/2=0.5,      # ** double star is used for exponential
print (round(2**0.5,4)) #4-digit round figure
 
print (3**0.5) #square root of 3
print (round(3**0.5,5))  #5 is used for round figures

print (3%2)  # % is module and also gives reminders

#exponential:
print(2**3)  #left to right
print (2**3**2) # if exponent is more than two, then presedence rule follow : right to left
#3**2=9
#2**9=512

print (2**4**1**3)

# 1**3=1
# 2**4**1
# 4**1=4
# 2**4=16
# output:16


