# **************summary of chapter one ***********

#print function ()
print ('hello')  #use single quote
print ("hello world") #use also double quote

#escape sequence
print ("hello \"world world")
print ("lineA \nlineB")

#escape sequence as normal text
print (r"hello \n world")

#python as calculator
print (2+3*5)
print (8+5-6)
print (2**3**2)  #exponetial form

#variables
name="Naveen kumar"
print (name)
name=12345
print (name)
 
#naming rules for variables
    #always start small letter and underscore
    #not start with number at beginning of declare of variable
    #not used special symbol as:@,$,& etc..

#covention for variable naming
  #use of snake case writting,
  # 
        #user_one    
        #user_naveen