#output: lineA \n lineB
#output:lineA \\n lineB
#output: lineA \\t lineB
#output: this is 4 backslash \\\\
#output: \" \'
  
print ("lineA \\n lineB")
print ("lineA \\\\n lineB")
print ("lineA \\\\t lineB")
print ("This is 4 backslash \\\\\\\\")
print ("\\\" \\\'")

# \" - "   (b)
# \\ - \   (a)
# \\\" - \"  (a+b)


# \' - '  (d)
# \\ - \   (c)
# \\\' - \'  (c+d)
